const paragraphs = document.querySelectorAll("p");
paragraphs.forEach((paragraph) => {
  paragraph.style.backgroundColor = "#ff0000";
});

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

const childNodes = optionsList.childNodes;
for (let node of childNodes) {
  console.log(node.nodeName, node.nodeType);
}

const testParagraph = document.querySelector(".testParagraph");
testParagraph.textContent = "This is a paragraph";

const header = document.querySelector(".main-header");
const listItems = header.querySelectorAll("li");

listItems.forEach((item) => {
  item.classList.add("nav-item");
  console.log(item);
});

const sectionTitles = document.querySelectorAll(".section-title");
sectionTitles.forEach((title) => {
  title.classList.remove("section-title");
});
